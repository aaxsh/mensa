import requests
from bs4 import BeautifulSoup
import re

_MENSA = r'''
  __  __ ______ _   _  _____
 |  \/  |  ____| \ | |/ ____|  /\
 | \  / | |__  |  \| | (___   /  \
 | |\/| |  __| | . ` |\___ \ / /\ \
 | |  | | |____| |\  |____) / ____ \
 |_|  |_|______|_| \_|_____/_/    \_\
                             by Aamir
'''

URLS = ['https://www.studentenwerk-magdeburg.de/mensen-cafeterien/mensa-unicampus/speiseplan-unten/', 'https://www.studentenwerk-magdeburg.de/mensen-cafeterien/mensa-unicampus/speiseplan-oben/',
'https://www.studentenwerk-magdeburg.de/mensen-cafeterien/mensa-kellercafe/speiseplan']

#parsing
def webwork(url):
    data = requests.get(url).content
    soup = BeautifulSoup(data, 'lxml')
    mensa_div = soup.find('table')
    mensa_div = mensa_div.findAll('tr')
    mensa_div.pop(0) #first part of table is useless
    mensa_div.pop() #last too
    return mensa_div

def work(mensa_div):
    eng_meals = []
    meals = []
    prices = []
    de_meals = []
    for mensa in mensa_div:
        eng_meal = mensa.find('span', {'class': 'grau'})
        eng_meals.append(eng_meal.text)
        de_meal = mensa.find('strong')
        meals.append(de_meal.text)
        price = str(mensa)
        price = re.search('</strong>(.+?)</td>', price) #prices are coded very bad -> extra work
        if price:
            price = price.group(1)
            price = price.replace('<br/>', '') #can't get price alone, so need to replace useless things
            prices.append(price)

    for index, elem in zip(meals, eng_meals):
        if elem != " ":
            de_meals.append(index.replace(elem, '')) #not able to get only german meals, so I created one english list and one with everything in. And then I replace the endglish one.
        else:
            de_meals.append(index)
    return de_meals, eng_meals, prices
def comapare(de_len, eng_len):
    if de_len > eng_len:
        return de_len
    return eng_len

de_meals_out = []
eng_meals_out = []
prices_out = []
for url in URLS:
    mensa_div = webwork(url)
    de_meals, eng_meals, prices = work(mensa_div)
    de_meals_out += de_meals
    eng_meals_out += eng_meals
    prices_out += prices

de_len = max(map(len, de_meals_out)) + 5
eng_len = max(map(len, eng_meals_out)) + 5
max_len = comapare(de_len, eng_len)

print(_MENSA)

for de_meal, eng_meal, price in zip(de_meals_out, eng_meals_out, prices_out):
    print(f'{de_meal:{max_len}s}{price}\n{eng_meal}')
